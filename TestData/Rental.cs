﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TestData
{
    class Rental
    {
        public int RentalId { get; set; }
        public int Distance { get; set; }
        public string CarId { get; set; }
        public int CustomerId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public override bool Equals(object obj)
        {
            
            Rental other = obj as Rental;
            if ((other.CarId == this.CarId || other.CustomerId == this.CustomerId) && (this.StartTime <= other.EndTime && other.StartTime <= this.EndTime))
            {
                return true;
            }
            return false;
        }
        public Rental(int rentalId, int distance, string carId, int customerId, DateTime startTime, DateTime endTime)
        {
            RentalId = rentalId;
            Distance = distance;
            CarId = carId;
            CustomerId = customerId;
            StartTime = startTime;
            EndTime = endTime;
        }
        public override int GetHashCode()
        {
            var hashCode = 713519861;
            hashCode = hashCode * -1521134295 + RentalId.GetHashCode();
            hashCode = hashCode * -1521134295 + Distance.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CarId);
            hashCode = hashCode * -1521134295 + CustomerId.GetHashCode();
            hashCode = hashCode * -1521134295 + StartTime.GetHashCode();
            hashCode = hashCode * -1521134295 + EndTime.GetHashCode();
            return hashCode;
        }
        public override string ToString()
        {
            string csv = string.Empty;
            csv += $"{RentalId};{Distance};\"{CarId}\";{CustomerId};{StartTime:yyyy-MM-dd HH:mm:ss};{EndTime:yyyy-MM-dd HH:mm:ss}";
            return csv;
        }
    }

    class RentalGeneration
    {
        public List<Car> Cars { get; set; }
        public List<Rental> Rentals { get; set; }
        private int index;
        public RentalGeneration(List<Car> cars)
        {
            Cars = cars;
            Rentals = new List<Rental>();
            rnd = new Random();
            index = 0;
        }
        public void ExportDSV(List<Rental> rentals)
        {
            StreamWriter sw = new StreamWriter("rentals2.dsv");
            string starterLine = $"\"RENTAL_ID\";\"DISTANCE\";\"CAR_ID\";\"CUSTOMER_ID\";\"START_TIME\";\"END_TIME\"";
            sw.WriteLine(starterLine);
            
            foreach (var item in rentals)
            {
                sw.WriteLine(item);
            }
            
            sw.Close();
        }
        private void GenerateRent()
        {
            int c = 0;
            for (int k = 0; k < Cars.Count; k++)
            {
                Car item = Cars[k];   
                for (int i = 0; i < 150; i++)
                {
                    int rentalId = ++index;
                    int customerId = rnd.Next(1, 15000);
                    string carId = item.CarId;
                    DateTime[] dates = GenerateDate();
                    double distance = Math.Round((dates[1].Subtract(dates[0]).TotalMinutes * 0.7), 0);
                    Rental r = new Rental(rentalId, (int)distance, carId, customerId, dates[0], dates[1]);
                    while (Rentals.Contains(r))
                    {
                        dates = GenerateDate();
                        distance = Math.Round((dates[1].Subtract(dates[0]).TotalMinutes * 0.7), 0);
                        r = new Rental(rentalId, (int)distance, carId, customerId, dates[0], dates[1]);
                    }
                    Rentals.Add(r);
                    c++;
                    double percent = ((double)c / (150 * Cars.Count))*100;
                    Console.WriteLine(i +"\t" + item.CarId + "\t" + c + "\t" + percent + "%");
                }
            }
            ExportJson(Rentals);


        }
        private DateTime[] GenerateDate()
        {
            DateTime first = new DateTime(2017, 1, 2, 0, 0, 0);
            DateTime start = first.AddDays(rnd.Next(DateTime.Now.Subtract(first).Days));
            DateTime end = start.AddMinutes(rnd.Next(10, 180));
            DateTime[] result = { start, end };
            return result;
        }
        public void ExportJson(List<Rental> rentals)
        {
            string jsonString = JsonConvert.SerializeObject(rentals, Formatting.Indented);
            StreamWriter sw = new StreamWriter("rentals.json");
            sw.WriteLine(jsonString);
            sw.Close();
        }
        private List<Rental> LoadFromJson(string jsonPath)
        {
            string jsonString = File.ReadAllText(jsonPath);
            return JsonConvert.DeserializeObject<List<Rental>>(jsonString);
        }
        public List<Rental> GenerateRentals(Option option)
        {
            if (option == Option.Generate)
            {
                GenerateRent();
                return LoadFromJson("rentals.json");
            }
            else
            {
                return LoadFromJson("rentals.json");
            }
        }
        static Random rnd;
       

        public List<Rental> CorrectRentals(List<Rental> rentals)
        {
            List<Rental> rentals2 = new List<Rental>();
            foreach (Rental rental in rentals)
            {
                int randHour = rnd.Next(2, 20);
                int randMinute = rnd.Next(50);
                rental.StartTime = rental.StartTime.AddHours(randHour).AddMinutes(randMinute);
                rental.EndTime = rental.EndTime.AddHours(randHour).AddMinutes(randMinute);
                rentals2.Add(rental);
            }

            return rentals2;
        }
    }
}
