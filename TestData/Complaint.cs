﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestData
{
    class Complaint
    {
        public int ComplaintId { get; set; }
        public int RentalId { get; set; }
        public string Details { get; set; }
        public override string ToString()
        {
            string csv = string.Empty;
            csv += $"{ComplaintId};{RentalId};\"{Details}\"";
            return csv;
        }
        public override bool Equals(object obj)
        {
            return this.RentalId == (obj as Complaint).RentalId; 
        }

        public override int GetHashCode()
        {
            int hashCode = -1910614522;
            hashCode = hashCode * -1521134295 + ComplaintId.GetHashCode();
            hashCode = hashCode * -1521134295 + RentalId.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Details);
            return hashCode;
        }
    }
    static class ComplaintGenerator
    {
        static string[] detailTypes =
        {
            "Nem megfelelo tisztasagu az auto.",
            "Baleset tortent, szemelyi serules nelkul.",
            "Meghuztak az autot amig parkolt.",
            "Baleset tortent szemelyi serulessel.",
        };
        static Random rnd = new Random();
        public static List<Complaint> GenerateComplaints(List<Rental> rentals, int count)
        {
            int idx = 1;
            int pcs = 0;
            List<Complaint> complaints = new List<Complaint>();
            while (pcs < count)
            {
                Complaint c = new Complaint();
                while (complaints.Contains(c))
                {
                    c = new Complaint()
                    {
                        RentalId = rentals[rnd.Next(rentals.Count)].RentalId,
                        Details = detailTypes[rnd.Next(detailTypes.Length)],
                    };
                }
                if (c.Details != string.Empty)
                {
                    c.ComplaintId = idx++;
                    complaints.Add(c);
                    pcs++;
                    Console.WriteLine(pcs);
                }
                
            }
            return complaints;
        }
        public static void ExportDSV(List<Complaint> complaints)
        {
            StreamWriter sw = new StreamWriter("complaints.dsv");
            string starterLine = $"\"COMPLAINT_ID\";\"RENTAL_ID\";\"DETAILS\"";
            sw.WriteLine(starterLine);

            foreach (var item in complaints)
            {
                sw.WriteLine(item);
            }

            sw.Close();
        }

    }
}
