﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestData
{
    class Invoice
    {
        public int InvoiceId { get; set; }
        public int RentalId { get; set; }
        public DateTime CompletionTime { get; set; }
        public int Amount { get; set; }
        public override string ToString()
        {
            string csv = string.Empty;
            csv += $"{InvoiceId};{RentalId};\"{CompletionTime:yyyy-MM-dd HH:mm:ss}\";{Amount}";
            return csv;
        }
    }
    static class InvoiceGenerator
    {
        static Random rnd = new Random();
        public static List<Invoice> GenerateInvoices(List<Rental> rentals)
        {
            int idx = 100;
            List<Invoice> invoice = new List<Invoice>();
            foreach (var rental in rentals)
            {
                Invoice i = new Invoice()
                {
                    InvoiceId = idx++,
                    RentalId = rental.RentalId,
                    CompletionTime = rental.EndTime.AddMinutes(rnd.Next(1, 15)),
                    Amount = 0,
                };
                invoice.Add(i);
                
            }
            return invoice;
        }
        public static void ExportDSV(List<Invoice> invoices)
        {
            StreamWriter sw = new StreamWriter("invoices.dsv");
            string starterLine = $"\"INVOICE_ID\";\"RENTAL_ID\";\"COMPLETION_TIME\";\"AMOUNT\"";
            sw.WriteLine(starterLine);

            foreach (var item in invoices)
            {
                sw.WriteLine(item);
            }

            sw.Close();
        }
    }
}
