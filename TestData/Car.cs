﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TestData
{
    enum Option
    {
        Generate, 
        JSON
    }
    class BrandModelPair
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public BrandModelPair(string brand, string model)
        {
            Brand = brand;
            Model = model;
        }
    }
    class Car
    {
        public string CarId { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public int Fuel { get; set; }
        public int LocationId { get; set; }
        public override bool Equals(object obj)
        {
            return (obj as Car).CarId == this.CarId;
        }
        public override int GetHashCode()
        {
            return CarId.GetHashCode();
        }
        public Car(string carId, string brand, string model, int fuel, int locationId)
        {
            CarId = carId;
            Brand = brand;
            Model = model;
            Fuel = fuel;
            LocationId = locationId;
        }
        public override string ToString()
        {
            string sql = $"INSERT INTO Cars (car_id, brand, model, fuel, location_id) VALUES ('{CarId}', '{Brand}', '{Model}', {Fuel}, {LocationId});";
            return sql;
        }
    }
    class CarGeneration
    {
        List<BrandModelPair> availableCars;
        List<Car> cars;
        public CarGeneration()
        {
            availableCars = new List<BrandModelPair>();
            availableCars.Add(new BrandModelPair("Volkswagen", "e-Golf"));
            availableCars.Add(new BrandModelPair("Volkswagen", "e-Up"));
            availableCars.Add(new BrandModelPair("Audi", "e-Tron"));
            availableCars.Add(new BrandModelPair("Nissan", "Leaf"));
            availableCars.Add(new BrandModelPair("BMW", "i3"));
            cars = new List<Car>();
            rnd = new Random();
        }
        static Random rnd;
        private void GenerateCars(int count)
        {
            
            for (int i = 0; i < count; i++)
            {
                BrandModelPair bmp = availableCars[rnd.Next(availableCars.Count)];
                Car c = new Car(GenerateRegistration(), bmp.Brand, bmp.Model, rnd.Next(15, 101), rnd.Next(1, 10));
                while (cars.Contains(c))
                {
                    c = new Car(GenerateRegistration(), bmp.Brand, bmp.Model, rnd.Next(15, 101), rnd.Next(1, 10));
                }
                cars.Add(c);
            }
            ExportToJson(cars);
        }

        private string GenerateRegistration()
        {
            string abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string carId = rnd.Next(10).ToString();
            for (int j = 0; j < 3; j++)
            {
                carId += abc[rnd.Next(abc.Length)].ToString();
            }
            carId += rnd.Next(100, 1000).ToString();
            return carId;
        }

        public void ExportSQL(List<Car> input)
        {
            StreamWriter sw = new StreamWriter("cars.sql");
            foreach (Car item in input)
            {
                sw.WriteLine(item);
            }
            sw.Close();
        }
        private void ExportToJson(List<Car> cars)
        {
            string jsonString = JsonConvert.SerializeObject(cars, Formatting.Indented);
            StreamWriter sw = new StreamWriter("cars.json");
            sw.WriteLine(jsonString);
            sw.Close();
        }
        private List<Car> LoadFromJson(string jsonPath)
        {
            string jsonString = File.ReadAllText(jsonPath);
            List<Car> carsFromJson = JsonConvert.DeserializeObject<List<Car>>(jsonString);
            return carsFromJson;
        }
        public List<Car> GetCars(Option option)
        {
            if (option == Option.Generate)
            {
                int count = int.Parse(Console.ReadLine());
                GenerateCars(count);
                return LoadFromJson("cars.json");
            }
            else
            {
                return LoadFromJson("cars.json");
            }
        }
       
        
    }
}
